#!/bin/bash

for f in *; do
	if [ -f $f ]; then
		head -1 $f
		echo $f
		tail -1 $f
	fi
done
